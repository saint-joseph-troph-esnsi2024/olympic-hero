from ursina import *
from game import init_game
from ursina.prefabs.dropdown_menu import DropdownMenu, DropdownMenuButton
import csv
from pathlib import Path, WindowsPath
from random import randint, choice
from os import path
package_folder = Path(__file__).parent
menu_name=""

class Cam(EditorCamera):
    def __ini__(self):#crée le fond d'écran qui tourne
        super().__init__() 
        self.eternal=True       
    def update(self):        
        self.rotation_z+=8

from obstacles import Obstacle
prekey=("",0)  #initialise une variable pour éviter NameError
class Bouton_touche(InputField): #définition de la classe Bouton_touche créant un champ de saisie personnalisé
    def __init__(self,texte_explicatif="",x=0 ,y=0, **kwargs): 
        super().__init__() #initialise InputField
        self.scale_x=0.4
        self.scale_y=0.05
        self.x=x
        self.y=y
        self.active=False              
        for key,value in kwargs.items():
            setattr(self,key,value)
        
        self.texte_explicatif=Text(text=texte_explicatif,y=y+0.1,x=x-0.05,color=color.black) #nom du champ affiché au dessus

    def input(self,key):  #prend toutes les entrées clavier              
        if self.active: 
            if key=='tab': #passer de champ en champ
                mouse.position = self.next_field.get_position(relative_to=camera.ui) #code copié de next_field
                invoke(setattr, self.next_field, 'active', True, delay=.01)
                self.active=False            
            list_touche=['up arrow', 'down arrow', 'left arrow', 'right arrow']        
            if key in list_touche: # écrit en appuyant sur les flèches
                self.text=key      
            
            if len(self.text)>0:
                if self.text[0] not in ['u','d','l','r']:
                    self.text=self.text[0]
                else:
                    bool_=False
                    for mot in list_touche: #permet l'ajout de caractères pour compléter les noms des flèches 
                        if self.text in mot:
                            bool_=True
                    if bool_==False:
                        self.text=self.text[:-1] #enlève un caractère si le caractère ne correspond pas
            
                         
    def get_key_(self): 
        """Donne la clé associée à celle saisie 
        Clés saisies en azerty données en qwerty"""
        valeur=self.text
        dico_key={' ' : ' ','q': 'a', 'w': 'z', 'e': 'e', 'r': 'r', 't': 't', 'y': 'y', 'u': 'u', 'i': 'i', 'o': 'o', 'p': 'p', 'a': 'q', 's': 's', 'd': 'd', 'f': 'f', 'g': 'g', 'h': 'h', 'j': 'j', 'k': 'k', 'l': 'l', 'z': 'w', 'x': 'x', 'c': 'c', 'v': 'v', 'b': 'b', 'n': 'n'}
        if valeur in ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'up arrow', 'down arrow', 'left arrow', 'right arrow']:
            if valeur in list(dico_key.keys()):
                return dico_key.get(valeur)
            return valeur        
        return self.default_value
    
    def update(self): 
        keys_=[' ','q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'up arrow', 'down arrow', 'left arrow', 'right arrow']
        global sauvegarder_touches
        try:
            if self.text[-1] not in keys_: #permet seulement quelques touches (celle détectables par input)
                self.text=self.text[:-1]
        except IndexError: pass
        if self.text not in keys_: # si la clé n'est pas correcte
            self.text_color=color.red           
            
        else:
            self.text_color=color.white 
    
    def is_ok(self): 
        """Pour Button_save; vérifie si les clés sont correctes"""      
        if self.text_color==color.white:return True





class Button_save(Button):
    def __init__(self,**kwargs):
        super().__init__()
        self.text_size=0.1
        for key,value in kwargs.items():
            setattr(self,key,value)
    
    def update(self):
        global input1,input2,input3,input4,texte_explicatif_erreur   
        input_list=[input1.text,input2.text,input3.text,input4.text]
        bool_double=False
        bool_ok=False
        for input in input_list:
            if input_list.count(input)>1: #si clés en double
                self.visible=False
                self.collider=None
                texte_explicatif_erreur.visible=True
                texte_explicatif_erreur.text="Les clés saisies sont en double"
                bool_double=True
                break              
        if input1.is_ok() and input2.is_ok() and input3.is_ok() and input4.is_ok(): #si syntaxe correcte
            bool_ok=True
            if bool_double==False: #si pas en double
                self.visible=True #affichage du bouton de sauvegarde
                self.collider='box'            
                texte_explicatif_erreur.visible=False
                texte_explicatif_erreur.text=""
                return #fin
        self.visible=False #sinon
        self.collider=None
        texte_explicatif_erreur.visible=True
        if bool_double==False and bool_ok==False: texte_explicatif_erreur.text="Les clés saisies sont inconnues"
        elif bool_double==True and bool_ok==False: texte_explicatif_erreur.text="Les clés saisies sont en double et les clés saisies sont inconnues"

class goto_field(InputField): #pour le débug
    def __init__(self,**kwargs):
        super().__init__() 
        for key,value in kwargs.items():
            setattr(self,key,value)       
        
    def input(self,key):
        """permet de se créer un raccouci pour aller au niveau souhaité, c'est pour tester les niveaux sans avoir a refaire tous les niveaux"""       
        if key=="enter":
            
            if self.text[:8]=="goto -> ":                
                if self.text[-1] in "0123456789":                    
                    nombre=self.text[-1]
                    if self.text[-2] in "0123456789":
                        nombre=self.text[-2]+nombre
                    print(nombre)
                    nombre=int(nombre)
                    if nombre<=30 and nombre>0: #si le nombre écrit est correcte, on réécrit le fichier puis on fait disparaître le champ
                        liste=[{"stat":"level "+str(c),"value": "done" if c<=nombre else "locked"} for c in range(1,31)]
                        for el in liste:
                            if int(el["stat"][-2:])==nombre: el["value"]="current"
                        with open("save.csv","w",encoding="utf8",newline="") as fichier_save:
                            text=csv.DictWriter(fichier_save,list(liste[0].keys()),delimiter=",")
                            text.writeheader()
                            text.writerows(liste)
                        self.visible=False
                        self.collider=None
                        self.active=False
                        self.text=""











def initlvl(a,niv):
    if niv.clickable==False:return
    global back_ground_sky
    back_ground_sky.eternal=False
    init_game(accueil)    
    file=open(a, encoding="utf8")
    obstacleList=list(csv.DictReader(file ,delimiter=","))
    
    for i in obstacleList:
        obstacle = Obstacle(nom_init=i['type'],dist=int(i['distance']),pos=int(i['position']))


def accueil():
    """créer l'accueil avec la musique""" 
    global back_ground_sky, goto_input,menu_name, bg_music
    scene.clear() 
    if menu_name=="":  
        with open("preferences.csv","r",encoding="UTF-8") as fi:
            volume=list(csv.DictReader(fi,delimiter=",") )[0]["Volume"]             
        bg_music = Audio(sound_file_name='bgm.mp3', autoplay=True, auto_destroy=False, loop = True, eternal = True, volume = float(volume))#lance la musique    
    if menu_name=="" or menu_name=="niveaux":
        back_ground_sky=Sky(texture=path.join('/textures/'+str(randint(1,10))+'_'+str(randint(1,10))+'_'+str(randint(1,10))+'.png'), scale=0.00001)
        back_ground_sky.eternal=True
    
        
    if not back_ground_sky:          
        back_ground_sky=Sky(texture=path.join('/textures/'+str(randint(1,10))+'_'+str(randint(1,10))+'_'+str(randint(1,10))+'.png'))
        back_ground_sky.eternal=True
      
    
    menu_name="accueil"  #permet de savoir le menu_name actuel mais surtout la provenance
    scene.clear() 
    DropdownMenu('Game', buttons=(DropdownMenuButton('Save'),DropdownMenuButton(text='Exit',on_click=exit)),color=color.black50)  #save ne sert à rien, c'est juste pour faire joli
    titre = Button(model='quad', texture='Titre.png', scale_x=0.7, scale_y=0.4, color=rgb(1,1,1,1), highlight_color=rgb(1,1,1,1), origin=(0,-0.7))
    play= Button(model="circle",scale=.3,icon_world_scale=.3,icon="bouton_play.png",color=rgb(0,0,0,0),text_size=1.9,text_color=color.black,origin=(0,0.4))
    play.clickable=True
    play.on_click = infini()
    play.on_click = lambda : initlvl(package_folder/"level/level_infinite.csv",play)
    #play.tooltip = Tooltip('play',background_color=rgb(0,0,0,0))
    reg =Button(model="circle",scale=.2,icon_world_scale=0.3,icon="bouton_reglage.png",color=rgb(0,0,0,0),pressed_scale=1.5,origin=(2,0.8))
    #reg.texture="bouton_reglage.png"
    reg.on_click = reglages #sans les parenthèses sinon elle est executée
    niv =Button(model="circle",scale=0.2,icon="bouton_niveau.png",icon_world_scale=10,color=rgb(0,0,0,0),text="NIVEAUX",text_size=0.3,text_color=color.black,pressed_scale=1.5,origin=(-2,0.8))
    niv.on_click= niveaux    
    background = Entity(model='plane',color=color.white, scale_x=5000, scale_z=5000)
    goto_input=goto_field(visible=False,collider=None) # ex:"goto -> 1" ex:"goto -> 11" ex:"goto -> 23"
    Cam()

def couleur_bouton(a):
    """dans la page des niveaux, crée met la couleurs des boutons, vert=fait, jaune=en cour, noir=non débloqué"""
    with open('save.csv', 'r', newline='') as save:
        lvllist = list(csv.DictReader(save,delimiter=','))
        if lvllist[a]['value'] == 'done':
            return 'bouton_vert',0
        elif lvllist[a]['value'] == 'current':
            return 'bouton_jaune',0
        else:            
            return 'bouton_noir',1
           

def niveaux(): 
    """crée la page des niveaux, s'initialise si on appuit sur le bouton niveau se trouvant dans l'accueil"""
    global back_ground_sky
    back_ground_sky.eternal=False   
    scene.clear()    
    global menu_name
    menu_name="niveaux"
    map = Button(model="quad", icon='map.png', scale_x=1.8, scale_y=1) 
    map.world_z=1000 #crée la texture du bouton, une map du monde
    #ici, c'est l'initialisation des bouttons pour les différents niveaux
    niv1 = Button(model="circle", icon=couleur_bouton(0)[0], scale=0.05,
                  pressed_scale=1.2, highlight_scale=1.1,
                  origin=(-1,5), color=rgb(0,0,0,0))
    niv1.clickable=not(bool(couleur_bouton(0)[1]))
    niv1.on_click = lambda : initlvl(package_folder/"level/level1.csv",niv1)
    niv2 = Button(model="circle", icon=couleur_bouton(1)[0], scale=0.05,
                  pressed_scale=1.2, highlight_scale=1.1,
                  origin=(0,2),color=rgb(0,0,0,0))
    niv2.clickable=not(bool(couleur_bouton(1)[1]))
    niv2.on_click = lambda: initlvl(package_folder/"level/level2.csv",niv2)
    niv3 = Button(model="circle", icon=couleur_bouton(2)[0], scale=0.05,
                  pressed_scale=1.2, highlight_scale=1.1,
                  origin=(6,2),color=rgb(0,0,0,0))
    niv3.clickable=not(bool(couleur_bouton(2)[1]))
    niv3.on_click = lambda: initlvl(package_folder/"level/level3.csv",niv3)
    niv4 = Button(model="circle", icon=couleur_bouton(3)[0], scale=0.05,
                  pressed_scale=1.2, highlight_scale=1.1,
                  origin=(0,-6),color=rgb(0,0,0,0))
    niv4.clickable=not(bool(couleur_bouton(3)[1]))
    niv4.on_click = lambda: initlvl(package_folder/"level/level4.csv",niv4)
    niv5 = Button(model="circle", icon=couleur_bouton(4)[0], scale=0.05,
                  pressed_scale=1.2, highlight_scale=1.1,
                  origin=(1,-3),color=rgb(0,0,0,0))
    niv5.clickable=not(bool(couleur_bouton(4)[1]))
    niv5.on_click = lambda: initlvl(package_folder/"level/level5.csv",niv5)
    retour= Button(model="circle",scale=0.05,icon="retour2.png",text_size=0.3,text_color=color.black,pressed_scale=0.7,origin=(15,-9))
    retour.on_click=accueil
    

def reglages():   
    """crée la page des églages si on appuit sur le boutton réglage dans l'accueil""" 
    scene.clear()
    global menu_name,slider,background__
    with open("preferences.csv","r",encoding="UTF-8") as fi: #permet de mettre les préférences sauvegardée
            volume=list(csv.DictReader(fi,delimiter=",") )[0]["Volume"]
    if menu_name=="controles":
        background__=Button(collider=None,color=color.white66,model="quad",scale_x=1.2,scale_y=0.9,origin=(0,0))
        background__.animate_scale_x(0.7,duration=0.3)        
        background__.animate("enabled",False,delay=0.4)
        background__.animate("visible",False,delay=0.4)        
        background__=Button(collider=None,color=color.white66,model="quad",scale_x=0.7,scale_y=0.9,origin=(0,0),visible=False)    
        background__.animate("visible",True,delay=0.37) 
    if menu_name=="accueil":
        background__=Button(collider=None,color=color.white66,model="quad",scale_x=0.7,scale_y=0.9,origin=(0,0),enabled=True)    
    
    menu_name="reglages" 

    #titre de la page
    Text.size=0.08
    nom=Text(text="Réglages", origin=(0,-4.5),color=color.black)
    #le boutton du bas, Personnaliser les contrôles
    Text.size=0.025
    touches = Button(model="quad",scale_x=0.4,scale_y=0.04,color=color.white,text="Personnaliser les contrôles",text_size=0.3,text_color=color.black,pressed_scale=1.5,origin=(0,6))
    touches.on_click=controles
    #le boutton en haut dans le coin de l'écran gauche pour revenir a l'accueil
    retour= Button(model="circle",scale=0.05,icon="retour2.png",text_size=0.3,text_color=color.black,pressed_scale=0.7,origin=(15,-9))
    retour.on_click=accueil
    #la barre de volume et son titre
    Text.size=0.04
    Text(text="Volume",origin=(0,-2),color=color.black)
    Text.size=0.025    
    slider = Slider(0, 10, default=float(volume)*10,bar_color=color.black, height=Text.size, step=0.5,  vertical=False, origin=(0,0),x=-0.25,color=color.red)
    slider.on_value_changed= lambda: change_vol(slider.value)
    #le boutton remettre a niveau
    reset_button=Button(model="quad",origin=(0,-5),scale_x=0.4,scale_y=0.04,text="Remettre à zéro les niveaux",text_size=1, color=color.white,text_color=color.black)
    reset_button.on_click=reset_levels


def change_vol(vol):
    """permet de sauvegader le changement du volume de la musique souhaitée"""
    global bg_music
    bg_music.volume=vol*0.1
    volume=bg_music.volume   
    with open("preferences.csv","r",encoding="UTF-8") as fi:
            contenu=list(csv.DictReader(fi,delimiter=",") )
    with open("preferences.csv","w",encoding="UTF-8",newline="") as fichier:
        write=csv.DictWriter(fichier,["Volume","Saut","Accroupi","Droite","Gauche"])
        write.writeheader()
        contenu[0]["Volume"]=volume
        write.writerow(contenu[0])
    


def reset_levels():
    """permet de reinitialiser les niveaux"""
    import csv
    liste=[{"stat":"level "+str(c),"value": "current" if c==1 else "locked"} for c in range(1,31)]
    with open("save.csv","w",encoding="utf8",newline="") as fichier_save:
        text=csv.DictWriter(fichier_save,list(liste[0].keys()),delimiter=",")
        text.writeheader()
        text.writerows(liste)
    x=Text(text="Les niveaux ont bien été remis à zéro", origin=(0,-6), color=color.black)
    x.animate('enabled',False,delay=1)

def controles():
    """crée la page des contrôles pour changer les touches isi on appuit sur la touche personnaliser les contrôles """
    global menu_name,background__
    menu_name="controles"    
    scene.clear()    
    background__=Button(collider=None,color=color.white66,model="quad",scale_x=0.7,scale_y=0.9,origin=(0,0))
    background__.animate_scale_x(1.2,duration=0.4) 
    Text.size=0.08
    #crée le titre réglage
    nom=Text(text="Réglages", origin=(0,-4.5),color=color.black)
    Text.size=0.025   
    global input1,input2,input3,input4,sauvegarder_touches,texte_explicatif_erreur
    #le boutton en haut dans le coin de l'écran gauche pour revenir a l'accueil
    retour= Button(model="circle",scale=0.05,icon="retour2.png",text_size=0.3,text_color=color.black,pressed_scale=0.7,origin=(15,-9))
    retour.on_click=reglages
    #permet d'entrer les préférences
    input1=Bouton_touche(texte_explicatif="Saut",default_value='up arrow',color=color.black, label='', max_lines=1, character_limit=11,text="up arrow", active=False,y=0.18,x=0)
    input2=Bouton_touche(texte_explicatif="Acrouppi",default_value='down arrow',color=color.black, label='', max_lines=1, character_limit=11,text="down arrow", active=False,y=-0.15,x=0)
    input3=Bouton_touche(texte_explicatif="Droite",default_value='right arrow',color=color.black, label='', max_lines=1, character_limit=11,text="right arrow", active=False,y=0,x=0.35)
    input4=Bouton_touche(texte_explicatif="Gauche",default_value='left arrow',color=color.black, label='', max_lines=1, character_limit=11,text="left arrow", active=False,y=0,x=-0.35)
    input1.next_field=input4 
    input4.next_field=input2
    input2.next_field=input3
    input3.next_field=input1
    #crée le boutton préférences qui va sauvegarder les celle-ci
    sauvegarder_touches=Button_save(model="quad",scale_x=0.4,scale_y=0.04,color=rgb(0,0,0,150),text="Sauvegarder les préférences",text_size=1,text_color=color.white,pressed_scale=1.5,origin=(0,7))
    sauvegarder_touches.on_click=write_control 
    texte_explicatif_erreur=Text(text="",visible=None,text_size=0.7,origin=(0,10),color=color.red)
    
    
    

def write_control():
    """sauvegarde les préférences"""
    global input1,input2,input3,input4,bg_music
    import csv 
    volume=bg_music.volume
    with open("preferences.csv","w",encoding="UTF-8",newline="") as fichier:
        write=csv.DictWriter(fichier,["Volume","Saut","Accroupi","Droite","Gauche"])
        write.writeheader()
        write.writerow({"Volume":volume,"Saut":input1.get_key_(),"Accroupi":input2.get_key_(),"Droite":input3.get_key_(),"Gauche":input4.get_key_()})
    reglages()

def input_(key):
    global menu_name
    
    if menu_name=="accueil":
        global prekey,goto_input  
        if prekey[0]=='escape' and key=='escape up':
            goto_input.visible=False
            goto_input.collider=None  
        list_keys=["up arrow","up arrow up","up arrow","up arrow up","right arrow","right arrow up","right arrow","right arrow up","down arrow","down arrow up","down arrow","down arrow up","left arrow","left arrow up","left arrow","left arrow up","t","t up","r","r up","o","o up","p","p up","h","h up","e","e up","e","e up","space","space up","n","n up","s","s up","i","i up"]
        if prekey[0]==list_keys[prekey[1]]==list_keys[-1]: 
            goto_input.visible=True
            goto_input.collider='box'
            goto_input.active=True
            prekey=(key,0)
            return 
        if prekey[0]==list_keys[prekey[1]]: prekey=(key,prekey[1]+1)
        else: prekey=(key,0)

def infini():
    """crée un level infini"""
    with open("level/level_infinite.csv","w",encoding="UTF-8",newline="") as fichier:
        write=csv.DictWriter(fichier,["type","distance","position"])
        write.writeheader()
        for i in range(300,7000,100):
            a=randint(0,4)
            obstaclelist=["colonne_haute", "colonne_rotation", "torche", "tour_eiffel", "arc_triomphe"]
            position=[-2,0,2]            
            if a == 4:
                write.writerow({"type" : "arc_triomphe", "distance" : i, "position" : 0})
            else:
                write.writerow({"type" : obstaclelist[a], "distance" : i, "position" : position[randint(0,2)]})