from ursina import Entity, color, texture_importer, Texture
from ursina.vec3 import Vec3
from random import choice
from settings import texture_path
import csv
from obstacles import Obstacle
from time import time
class Player(Entity):  #  création d'un joueur
    
    def __init__(self, **variables_donnees):
        
        super().__init__()
        self.time_counter=time()
        self.position=Vec3(-0.6,2,0)
        self.collider='box'    
        self.model='complet.glb' #forme du joueur        
        self.scale= 20 # hauteur du joueur
        
        
        for key, value in variables_donnees.items(): #utilise les arguments données lors de l'appel de la classe pour initialiser
            setattr(self, key, value)
    
        self.rotation_y=-90
        self.rotation_x=-90
    
    def input(self, key):
        with open("preferences.csv","r",encoding="UTF-8") as fi:
            keys_player=list(csv.DictReader(fi,delimiter=",") )
        UP=keys_player[0]["Saut"]
        DOWN=keys_player[0]["Accroupi"] 
        RIGHT=keys_player[0]["Droite"] 
        LEFT=keys_player[0]["Gauche"]    
        if key == LEFT: #changement vers la gauche de position            
            if round(self.x,1) in (1.4,-0.6):
                self.animate_x(round(self.x-2,1), duration=0.1)
        
        elif key == RIGHT: #changement vers la droite de position
            
            if round(self.x,1) in (-2.6,-0.6): 
                self.animate_x(round(self.x+2,1), duration=0.1)  
            
        
                
        elif key == UP and self.y<2.5: # mécanisme du saut ( self.y<2.5 : évite le spam de la touche 'up arrow' )       
            self.animate_y(5,duration=0.1)
            self.animate_y(2,duration=0.1, delay=0.19)
        

        elif key == DOWN and self.scale_z>19 and round(self.y,1)==2: #mécanisme 2            
            self.scale_z=17            
            self.collider=None
            self.collider='box'
            self.y=1.5
            self.animate_y(2,delay=0.2)
            self.animate_scale_z(20,delay=0.2) 
    def update(self):
        if self.time_counter+36<time(): #toutes les 36 secondes 
                print(self.time_counter,time())              
                self.time_counter=time()                                 
                for i in range(300,7000,100):                        
                    obstaclelist=["colonne_haute", "colonne_rotation", "torche", "tour_eiffel", "arc_triomphe"]
                    position=[-2,0,2]                    
                    Obstacle(nom_init=choice(obstaclelist),dist=i,pos=choice(position))           
        
        
if __name__=="__main__":
    from ursina import Ursina, EditorCamera
    app=Ursina()
    Player()
    EditorCamera(rotation_x=11)
    Entity(model='plane',color=color.white, scale_x=10, scale_z=3000, position=(0,0,1400),texture='grass')
    app.run()     
   