from ursina import Entity, color, time, duplicate, destroy
from random import choice, randint
from time import sleep

c=0
speed=200
randomness=0
entites_list=[]
class Obstacle(Entity): 
    
    def resize(self,x): 
        self.scale = self.scale*x

        
    def __init__(self, nom_init="",dist=0,pos=0, **variables_donnees):   #   variables_donnees est un dico contenant toutes les infos données avec une variable ex: position=(3,3,3) le dico est souvent appellé kwargs 
        global entites_list
        super().__init__()
        #reset
        self.nom=nom_init
        self.z=300+dist
        self.texture=None
        self.collider=None
        self.color=None
        self.visible=True
        self.do_update=False
        self.model=None
        self.x=0
        self.y=0
        #init
        if nom_init=='arc_triomphe': #ex pour la génération nom_init=='arc_triomphe' or num=1
            self.model='arc_de_triomphe_test_export.bam'
            
            self.scale_y=50*0.001
            self.scale_z=22*0.001
            self.scale_x=50*0.001

            obstacle_1 = Entity(parent=self, model='cube', collider='box', position=(self.x+16.25, self.y+22.5, self.z+10),scale=(12.5,45,22), color=color.white) #crée des enfants pour les collisions
            obstacle_2 = Entity(parent=self, model='cube', collider='box', position=(self.x-16.25, self.y+22.5, self.z+10), scale=(12.5,45,22), color=color.white)
            obstacle_3 = Entity(parent=self, model='cube', collider='box', position=(self.x, self.y+47.5, self.z+10), scale=(45,5,22), color=color.white)            
            self.resize(2.5)
            self.visible=True
            bool_visible=False
            obstacle_1.visible=bool_visible #juste pour le debug
            obstacle_2.visible=bool_visible
            obstacle_3.visible=bool_visible
            self.color=color.white
            self.texture=None
            self.color=None            
            self.texture='UV_arc' #censé être une texture UV mais c'est trop compliqué à faire
            #self.texture_scale=(50,45)
            self.texture_scale=(0.05,0.045)
            self.do_update=True
            self.x=pos  
        
        elif nom_init=='torche':
            self.model='torche_flamme.bam'
            self.visible=True            
            self.collider='box'
            self.do_update=True
            self.resize(0.4)
            self.y=2
            self.texture='texture_torche1.png'
            self.texture_scale=(0.1,1)
            self.x=pos
            
        elif nom_init=='tour_eiffel':
            self.model='tour_eiffel_1.bam'            
            self.visible=True            
            self.texture=None
            self.color=color.rgb(100,100,100,255)
            self.resize(0.01)
            self.collider='box'
            self.y=0
            self.do_update=True
            self.x=pos
            
            
        elif nom_init=="colonne_rotation":
            self.model='colonne_grecque.bam'
            self.visible=True
            self.color=None
            self.texture=None
            self.color=color.white
            self.resize(0.05)
            self.collider='box'
            self.y=0
            self.rotation_x=90
            self.do_update=True
            self.x=pos
            
        elif nom_init=="colonne_haute":
            self.model='colonne_grecque.bam'
            self.visible=True
            self.color=None
            self.texture=None
            self.color=color.white
            self.resize(0.05)
            self.collider='box'
            self.y=0
            self.do_update=True
            self.x=pos
        
        elif nom_init=="end":            
            self.model='end.obj' #forme de l'obstacle
            self.collider='box'
            self.visible=True    
            self.color=color.black50
            self.do_update=True
            self.scale=0.08
            self.x=5
            self.rotation_y=-90
        
        else:
            self.model='cube' #forme de l'obstacle
            self.collider='box'
            self.color=None  
            self.texture=None     
            self.color = color.red #couleur
            self.scale_y = 1 #fixe les dimentions de l'obstacle
            self.scale_x = 2.8
            self.scale_z = 1
            self.do_update=True
            self.x=pos
            
        for key, value in variables_donnees.items(): 
            setattr(self, key, value) #établit les attributs pour chaque élément dans le dico variables_donnees
        entites_list.append(self)
           
    
    
       
    def update(self):  # est exécutée tout le tps (à intervalles réguliers)   durée des intervalles   
        global c, speed, entites_list
        
        if self.do_update==False:   #exclut *destroyed entity*   variable créée, donc la valeur est à changer manuellement   
            return 
        try:
            pos_z=self.z
        except AssertionError: #évite les AssertionError si self.z en fait une (cas où le  self.do_update==False n'a pas marché)
            return 
        #détection des collisions
        hit_info=False 
        for sous_entite in self.children: #cherche une collision dans les enfants de l'obstacle (actuellement : pour l'arc de triomphe)
            hit_info = (sous_entite.intersects(ignore=self.children).hit or hit_info)
            
            
        try: #évite les erreurs car l'arc de triomphe possède un MeshCollider qui ne peut pas être détecté avec cette technique 
            hit_info = self.intersects().hit or hit_info           
        
        except Exception as e:
            print(e)
        
        if hit_info==False:   #si il n'y a pas contact z=self.getZ()
            try:                
                pos_z+= time.dt * speed * (-1)
                self.setZ(pos_z)
            except AssertionError as e:                
                print("ERROR", e)         
            for sous_entite in self.children:
                sous_entite.z=pos_z
       
        if hit_info==True: #si il y a contact
            print('Hit') #GAME OVER        
            try:                
                pos_z+= time.dt * speed * (-1)
                self.setZ(pos_z)
            except AssertionError as e:
                print(e)
            for sous_entite in self.children:
                sous_entite.z=pos_z             
        #fin des collisions
        if self.z<-20:
            destroy(self, delay=0)
            self.do_update=False
            return
        if self.z>400:
            self.visible=False
            if self.nom!="arc_triomphe": self.collider=None
        else: 
            self.visible=True
            if self.nom!="arc_triomphe": self.collider="box"            
        

        c+=1         
if __name__=="__main__":
    from ursina import *
    app=Ursina()
    Obstacle(nom_init="end")
    EditorCamera(rotation_x=11)
    Entity(model='plane',color=color.red, scale_x=10, scale_z=3000, position=(0,0,1400),texture='grass')
    app.run()