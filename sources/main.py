from menu import accueil,input_
from ursina import Ursina, window, Audio
from os.path import exists

import csv

app = Ursina(title="Test",  #nomme la fenêtre
             use_ingame_console=False, #affiche la console dans le jeu
            fullscreen=False, 
            development_mode=False) 

if exists('save.csv') is False: #vérifie si un fichier de sauvegarde existe, en créé un si il n'existe pas
    with open('save.csv',"w",encoding="utf-8",newline="") as output: 
        item=csv.DictWriter(output,['stat','value'])
        item.writeheader()
        item.writerows([{"stat":"level "+str(c),"value": "current" if c==1 else "locked"} for c in range(1,31)])

file=open("save.csv", encoding="utf8")
playerStat=list(csv.DictReader(file ,delimiter=","))
levelCleared=playerStat[0]["value"]
lvl=levelCleared 

accueil() #initialise l'accueil
def input(key): #envoie les entrées claviers à une fonction du fichier menu
    input_(key)
    


#début déactivation des informations
window.fps_counter.enabled = False 
window.entity_counter.enabled = False
window.collider_counter.enabled = False
window.exit_button.enabled = True
#fin des déactivation

app.run() #lance le jeu

