from ursina import Entity,EditorCamera, Sky,Vec3, color, DirectionalLight, scene, Button, application
from joueur import Player
from ursina.shaders import lit_with_shadows_shader
from pathlib import Path

package_folder = Path(__file__).parent #chemin actuel vers le fichier



def init_game(fonction) -> None:    
    """ Sert à initialiser le jeu
    Prend en entrée une fonction qui sert à la flèche de retour
    """
    scene.clear() 
    player = Player() # initialise un joueur 
    player.shader=lit_with_shadows_shader #enlève les effets d'ombres   
    #sol
    ground = Entity(model='plane',color=color.white, scale_x=10, scale_z=3000, position=(0,0,1400)) # crée un sol
    ground.texture='grass' #pas du tout ce qui restera à la fin : pas les droits 
    ground.texture_scale=(1,150) #fixe la taille de la texture du sol
    #fin sol

    sky=Sky(color=color.blue) #crée un ciel (bleu) 

    cam=EditorCamera() #met un place un camera pour le dev (rotation -> clic gauche  mouvement -> clic roulette  zoom -> roulette)
    cam.rotation_x=10 #definit un angle de camera pour le début
    cam.z=-3
    cam.y=2    

    #lumière
    lum=Entity(rotation_x=90,model='cube', y=10,x=0,z=-10)
    lum.visible=False
    light=DirectionalLight(parent=lum,  shadows=False) #dirige la lumière
    light.look_at(Vec3(0,3,5))
    # fin lumière
    retour= Button(model="circle",scale=0.05,icon="retour2.png",
                   text_size=0.3,text_color=color.black,pressed_scale=0.7,origin=(15,-9))    
    retour.on_click=fonction #bouton de retour
    

    
